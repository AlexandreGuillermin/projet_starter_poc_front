import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdministratorPageRoutingModule } from './administrator-routing.module';

import { AdministratorPage } from './administrator.page';
import { SharedModule } from '../shared/shared.module';
import { ChangeUserManagerComponent } from './change-user-manager/change-user-manager.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdministratorPageRoutingModule,
    SharedModule
  ],
  declarations: [AdministratorPage, ChangeUserManagerComponent]
})
export class AdministratorPageModule { }
