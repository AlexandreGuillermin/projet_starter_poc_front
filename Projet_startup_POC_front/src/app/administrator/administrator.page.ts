import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.page.html',
  styleUrls: ['./administrator.page.scss'],
})
export class AdministratorPage implements OnInit {

  displayedView: string = "default";

  constructor(private menu: MenuController) { }

  ngOnInit() { }

  /*Close left side menu*/
  openEnd() {
    this.menu.close();
  }

  /*The following functions is used by the left side menu buttons*/

  public changeUserManagerEvent() {
    this.displayedView = "changeUserManager";
  }
}
