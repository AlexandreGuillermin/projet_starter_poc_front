import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/api.service';

@Component({
  selector: 'app-change-user-manager',
  templateUrl: './change-user-manager.component.html',
  styleUrls: ['./change-user-manager.component.scss'],
})
export class ChangeUserManagerComponent implements OnInit {

  //Form fields
  changeForm: any = {};
  apiService: ApiService = new ApiService(this.httpApi);

  constructor(private httpApi: HttpClient) { }

  ngOnInit() { }

  /*Change le manager d'un user*/
  public changeUserManager() {
    this.apiService.patchManagerOfUser(this.changeForm).pipe(
      tap(async response => {
        if (response) {
          alert("Manager changed");
        }
        else {
          alert("Manager not found")
        }
      }
      )).subscribe();
  }
}
