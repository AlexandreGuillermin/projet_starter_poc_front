import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router, RouterEvent } from '@angular/router';
import { Storage } from '@ionic/Storage';

import { AuthentificationService } from './shared/login/authentification.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  displayedView = "consultUser";

  private selectedPath: String

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private authenticationService: AuthentificationService,
    private storage: Storage
  ) {
    //initialize App
    this.initializeApp();
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      //manage redirection according to the role and the authentification case
      this.authenticationService.authState.subscribe(state => {
        this.storage.get("isConnected").then(
          response => {
            if (response == "true") {
              console.log(this.authenticationService.role, " est connecté");
              try {
                if (this.authenticationService.role == "ADMIN") {
                  this.router.navigateByUrl("administrator");
                }
                if (this.authenticationService.role == "MANAGER") {
                  this.router.navigateByUrl("manager");
                }
                if (this.authenticationService.role == "USER") {
                  this.router.navigateByUrl("user");
                }
              }
              catch {
                console.log("Invalid role");
              }
            } else {
              this.router.navigateByUrl('login');
            }
          }
        )
      });
    });
  }

  public consultUserEvent() {
    this.displayedView = "consultUser";
  }

  public consultProjectEvent() {
    this.displayedView = "consultProject";
  }
}
