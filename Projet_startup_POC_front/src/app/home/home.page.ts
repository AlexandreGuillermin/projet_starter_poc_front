import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private menu: MenuController,
    public alertCtrl: AlertController
  ) { }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }
}
