import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Storage } from '@ionic/Storage';
import { NgForm } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/api.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {

  @Output()
  pageChangeEvent = new EventEmitter<string>();

  private apiService: ApiService = new ApiService(this.httpClient);

  /*Form field */
  userForm: any = {}

  constructor(private storage: Storage, private httpClient: HttpClient) { }

  ngOnInit() { }

  /*Create a user*/
  onClickAdd() {
    this.storage.get("idUser").then(
      managerId => {
        //complete the user json (will be send to API)
        this.userForm.idManager = managerId;
        console.log(this.userForm);
        this.apiService.postUser(this.userForm).pipe(
          tap(async userId => {
            if (userId != null) {
              //redirection to the "consult user" page
              this.pageChangeEvent.emit("consultUser");
            }
          }
          )).subscribe();
      }
    )
  }
}
