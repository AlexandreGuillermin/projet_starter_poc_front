import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/shared/api.service';

@Component({
  selector: 'app-consult-project',
  templateUrl: './consult-project.component.html',
  styleUrls: ['./consult-project.component.scss'],
})
export class ConsultProjectComponent implements OnInit {

  @Output()
  pageChangeEvent = new EventEmitter<string>();
  projectList$: Observable<any>;
  private apiService: ApiService = new ApiService(this.httpClient);

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    this.projectList$ = this.apiService.getAllProjects();
  }

  /*Create project event */
  onClickAdd() {
    this.pageChangeEvent.emit("createProject");
  }
}
