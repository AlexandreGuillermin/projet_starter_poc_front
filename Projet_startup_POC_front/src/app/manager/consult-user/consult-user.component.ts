import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/Storage';
import { ApiService } from 'src/app/shared/api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-consult-user',
  templateUrl: './consult-user.component.html',
  styleUrls: ['./consult-user.component.scss'],
})
export class ConsultUserComponent implements OnInit {

  @Output()
  pageChangeEvent = new EventEmitter<string>();
  @Output()
  consultUserTimeEvent = new EventEmitter<any>();

  userList$: Observable<any>;
  private apiService: ApiService = new ApiService(this.httpClient);

  constructor(private storage: Storage, private httpClient: HttpClient) { }

  ngOnInit() {
    //get observer of user list from API
    this.storage.get("idUser").then(
      managerId => {
        console.log("Manager id :", managerId);
        this.userList$ = this.apiService.getUserOfManagerId(managerId);
      }
    )
  }

  /*Redirection to add user page */
  onClickAdd() {
    this.pageChangeEvent.emit("addUser");
  }

  /*Redirection to "consult user time" page*/
  selectedUser($event) {
    let event = $event;
    this.consultUserTimeEvent.emit(event);
  }
}
