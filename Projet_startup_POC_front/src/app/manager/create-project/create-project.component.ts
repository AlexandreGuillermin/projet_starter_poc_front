import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { tap } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/api.service';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss'],
})
export class CreateProjectComponent implements OnInit {

  @Output()
  pageChangeEvent = new EventEmitter<string>();

  private apiService: ApiService = new ApiService(this.httpClient);

  /*Form field */
  projectName: string;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() { }

  /*Create a new project */
  onClickAdd() {
    //project creation
    this.apiService.postProject(this.projectName).pipe(
      tap(async project => {
        if (project.name == this.projectName) {
          //redirection to "consult project" page
          this.pageChangeEvent.emit("consultProject");
        }
      }
      )).subscribe();
  }
}
