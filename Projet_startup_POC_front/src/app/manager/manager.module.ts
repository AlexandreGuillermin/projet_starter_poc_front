import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManagerPageRoutingModule } from './manager-routing.module';

import { IonicStorageModule } from '@ionic/storage';

import { ManagerPage } from './manager.page';
import { SharedModule } from '../shared/shared.module';
import { ConsultUserComponent } from './consult-user/consult-user.component';
import { UserListComponent } from './consult-user/user-list/user-list.component';
import { ConsultProjectComponent } from './consult-project/consult-project.component';
import { AddUserComponent } from './add-user/add-user.component';
import { CreateProjectComponent } from './create-project/create-project.component';
import { ProjectListComponent } from './consult-project/project-list/project-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManagerPageRoutingModule,
    SharedModule,
    IonicStorageModule.forRoot(),
  ],
  declarations: [ManagerPage,AddUserComponent,ConsultProjectComponent,ProjectListComponent,ConsultUserComponent,UserListComponent,CreateProjectComponent]
})
export class ManagerPageModule {}
