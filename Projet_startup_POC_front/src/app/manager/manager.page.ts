import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.page.html',
  styleUrls: ['./manager.page.scss'],
})
export class ManagerPage implements OnInit {

  displayedView: string = "default";
  userToConsultTime: any;

  constructor(private router: Router, private menu: MenuController) { }

  ngOnInit() { }

  /*When the manager want to consult the time for a selected user */
  consultUserTimeEventHander($selectedUser) {
    this.userToConsultTime = $selectedUser;
    this.consultUserTimeEvent();
  }

  /*Manage navigation into manager page */
  public pageChangeEventHander($event: any) {
    this.displayedView = $event;
  }

  /*====The following functions are used by the left side menu buttons====*/

  public consultProjectTimeEvent() {
    this.displayedView = "consultProjectTime";
  }

  public consultUserEvent() {
    this.displayedView = "consultUser";
  }

  public consultUserTimeEvent() {
    this.displayedView = "consultUserTime";
  }

  public consultProjectEvent() {
    this.displayedView = "consultProject";
  }

  public createProjectEvent() {
    this.displayedView = "createProject";
  }

  public consultProjectUserEvent() {
    this.displayedView = "consultProjectUser";
  }

  public addUserEvent() {
    this.displayedView = "addUser";
  }

  /*Close left side menu*/
  openEnd() {
    this.menu.close();
  }
}
