import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

const requestOption = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  //constants
  constBackProtocol: string = "http://";
  constBackIpAdress: string = "localhost";
  constBackPortNumber: string = "8080";
  constUserBackUri: string = "/user"
  constUsersBackUri: string = "/users"
  constManagerBackUri: string = "/manager"
  constDailyTimesBackUri: string = "/dailyTimes"
  constNewUserBackUri: string = "/new_users"
  constProjectBackUri: string = "/projects"

  constructor(
    public httpApi: HttpClient
  ) { }

  /*Login call API */
  public login(login: string, password: string) {
    return this.httpApi.post(this.constBackProtocol + this.constBackIpAdress + ':' + this.constBackPortNumber + this.constUsersBackUri, {
      login,
      password
    },
      requestOption
    ).pipe(
      map((response: any) => ([response.role.title, response.idUser])),
      catchError(() => of(false))
    );
  }

  public getUserOfManagerId(managerId: Number) {
    return this.httpApi.get(this.constBackProtocol + this.constBackIpAdress + ':' + this.constBackPortNumber + this.constUsersBackUri + "?idManager=" + managerId, requestOption
    ).pipe(
      map((response: any) => response)
    );
  }

  public getUserById(userId: Number) {
    return this.httpApi.get(this.constBackProtocol + this.constBackIpAdress + ':' + this.constBackPortNumber + this.constUserBackUri + "?idUser=" + userId, requestOption
    ).pipe(
      map((response: any) => response)
    );
  }

  public postUser(userForm: any,) {
    return this.httpApi.post(this.constBackProtocol + this.constBackIpAdress + ':' + this.constBackPortNumber + this.constNewUserBackUri, userForm, requestOption
    ).pipe(
      map((response: any) => response)
    );
  }

  public patchManagerOfUser(changeForme: any) {
    return this.httpApi.patch(this.constBackProtocol + this.constBackIpAdress + ':' + this.constBackPortNumber + this.constManagerBackUri, changeForme, requestOption
    ).pipe(
      map((response: any) => response)
    );
  }

  public getDailyTimeOfUser(userId: any) {
    return this.httpApi.get(this.constBackProtocol + this.constBackIpAdress + ':' + this.constBackPortNumber + this.constDailyTimesBackUri + "?idUser=" + userId, requestOption
    ).pipe(
      map((response: any) => response)
    );
  }

  public postNewDailyTime(userForm: any,) {
    return this.httpApi.post(this.constBackProtocol + this.constBackIpAdress + ':' + this.constBackPortNumber + this.constDailyTimesBackUri, userForm, requestOption
    ).pipe(
      map((response: any) => response)
    );
  }

  public getAllProjects() {
    return this.httpApi.get(this.constBackProtocol + this.constBackIpAdress + ':' + this.constBackPortNumber + this.constProjectBackUri, requestOption
    ).pipe(
      map((response: any) => response)
    );
  }

  public postProject(projectName: string) {
    return this.httpApi.post(this.constBackProtocol + this.constBackIpAdress + ':' + this.constBackPortNumber + this.constProjectBackUri,
      projectName,
      requestOption
    ).pipe(
      map((response: any) => response)
    );
  }
}
