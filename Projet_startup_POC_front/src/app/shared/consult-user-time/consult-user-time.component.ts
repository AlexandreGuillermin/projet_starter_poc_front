import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/Storage';
import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/api.service';
import { DailyTime } from 'src/app/shared/daily-time';


@Component({
  selector: 'app-consult-user-time',
  templateUrl: './consult-user-time.component.html',
  styleUrls: ['./consult-user-time.component.scss'],
})
export class ConsultUserTimeComponent implements OnInit {

  @Input()
  public userToConsultTime: any;

  private dailyTimes: Array<DailyTime>;
  private apiService: ApiService = new ApiService(this.httpClient);

  constructor(private storage: Storage, private httpClient: HttpClient) { }

  ngOnInit() { }

}
