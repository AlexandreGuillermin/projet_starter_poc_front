import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Storage } from '@ionic/Storage';
import { AlertController, Platform } from '@ionic/angular';
import { ApiService } from 'src/app/shared/api.service';
import { AuthentificationService } from './authentification.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    public authentificationService: AuthentificationService
  ) {}

  /*This function specify if an user can access a specific page */
  canActivate(): boolean {
    return this.authentificationService.isAuthenticated();
  }
}
