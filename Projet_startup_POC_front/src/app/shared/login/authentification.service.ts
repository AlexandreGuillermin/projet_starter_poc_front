import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/api.service';
import { Storage } from '@ionic/Storage';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  login$: Subscription
  authState = new BehaviorSubject(false);
  role: string;

  constructor(
    private storage: Storage,
    private platform: Platform,
    public router: Router,
    public apiService: ApiService,
    public alertCtrl: AlertController
  ) {
    //Load browser information when it is ready
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
  }

  /*Login management */
  login(email: string, password: string) {
    //call the API
    this.login$ = this.apiService.login(email, password).pipe(
      tap(async user => {
        if (user != false) {
          let role = user[0];
          let idUser: string = user[1];
          //saving user's data
          this.storage.set("role", role).then(
            response1 => {
              this.role = role;
              this.storage.set("isConnected", "true").then(
                response2 => {
                  this.storage.set("idUser", idUser).then(
                    response3 => {
                      //redirection
                      this.authState.next(true);
                    }
                  )
                }
              )
            }
          )
        } else {
          //error message
          const alert = await this.alertCtrl.create({
            header: 'Alert',
            message: 'Email ou mot de passe invalide',
            buttons: ['OK']
          });
          await alert.present();
        }
      })
    ).subscribe();
  }

  /*Switch both browser data and object statut */
  logout() {
    console.log("***Déconnexion***");
    this.storage.set("role", "none").then(
      response1 => {
        this.role = "none";
        this.storage.set("isConnected", "false").then(
          response2 => {
            this.authState.next(false);
          }
        )
      }
    )

  }

  /*Load authentification information in browser data*/
  ifLoggedIn() {
    //get statut
    this.storage.get("isConnected")
      .then((response) => {
        if (response) {
          console.log("Le storage indique connecté");
          this.authState.next(true);
        }
      });
    //get role
    this.storage.get("role").then((response) => {
      this.role = response;
    });
  }

  isAuthenticated() {
    return this.authState.value;
  }

  ngOnDestroy(): void {
    this.login$.unsubscribe();
  }
}
