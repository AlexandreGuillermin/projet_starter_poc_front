import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from './authentification.service';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  //form fields
  email: string;
  password: string;

  //Connexion state (switch display)
  userIsConnected: Boolean;

  //this parameters have to be set here by the framework
  constructor(public authentificationService: AuthentificationService, private menu: MenuController) {
    this.userIsConnected = this.authentificationService.authState.value;
    //this observer will automaticly change the display
    this.authentificationService.authState.subscribe(state => {
      this.userIsConnected = state;
    })
  }

  ngOnInit() { }

  login() {
    this.authentificationService.login(this.email, this.password);
  }

  logout() {
    this.menu.close();
    this.authentificationService.logout();
  }
}
