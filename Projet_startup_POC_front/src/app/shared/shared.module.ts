import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginComponent } from './login/login.component';
import { IonicStorageModule } from '@ionic/storage';
import { ConsultUserTimeComponent } from './consult-user-time/consult-user-time.component';


@NgModule({
  declarations: [LoginComponent, ConsultUserTimeComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicStorageModule.forRoot(),
  ],
  exports: [
    LoginComponent,
    ConsultUserTimeComponent,
  ]
})
export class SharedModule { }
