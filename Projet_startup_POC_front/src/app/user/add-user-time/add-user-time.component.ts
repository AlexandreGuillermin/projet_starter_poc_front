import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/Storage';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { tap } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/api.service';

@Component({
  selector: 'app-add-user-time',
  templateUrl: './add-user-time.component.html',
  styleUrls: ['./add-user-time.component.scss'],
})
export class AddUserTimeComponent implements OnInit {

  @Output()
  pageChangeEvent = new EventEmitter<string>();

  private apiService: ApiService = new ApiService(this.httpClient);

  /*Form field */
  dailyForm: any = {}
  choosenProjectId: any;
  projects: any = [];

  constructor(private storage: Storage, private httpClient: HttpClient) { }

  ngOnInit() {
    //load all the project in memory
    this.apiService.getAllProjects().pipe(
      tap(async projects => {
        if (projects != null) {
          console.log(projects);
          this.projects = projects;
        }
      }
      )).subscribe();
  }

  /*Add a time to the user*/
  onClickAdd() {
    this.storage.get("idUser").then(
      userId => {
        //complete the user json
        this.dailyForm.idUser = userId;
        this.dailyForm.idProject = this.choosenProjectId;
        console.log(this.dailyForm);
        //send to api 
        this.apiService.postNewDailyTime(this.dailyForm).pipe(
          tap(async dailyTimeId => {
            if (dailyTimeId != null) {
              //redirection to the "consult user time" page
              this.pageChangeEvent.emit("consultUserTime");
            }
          }
          )).subscribe();
      }
    )
  }
}
