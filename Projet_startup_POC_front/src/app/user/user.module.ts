import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';

import { UserPageRoutingModule } from './user-routing.module';

import { UserPage } from './user.page';
import { SharedModule } from '../shared/shared.module';
import { AddUserTimeComponent } from './add-user-time/add-user-time.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserPageRoutingModule,
    SharedModule,
    IonicStorageModule.forRoot()
  ],
  declarations: [UserPage, AddUserTimeComponent]
})
export class UserPageModule { }
