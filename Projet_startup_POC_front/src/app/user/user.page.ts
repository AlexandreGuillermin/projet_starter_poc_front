import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/Storage';
import { tap } from 'rxjs/operators';
import { ApiService } from '../shared/api.service';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {

  displayedView: string = "default";
  userToConsultTime: any;

  private apiService: ApiService = new ApiService(this.httpApi)

  constructor(private storage: Storage, private httpApi: HttpClient, private menu: MenuController) { }

  ngOnInit() {
    this.loadUser();
  }

  /*Get id from connected user */
  public loadUser() {
    this.storage.get("idUser").then(
      userId => {
        this.apiService.getUserById(userId).pipe(
          tap(async user => {
            this.userToConsultTime = user;
          }
          )).subscribe();
      }
    )
  }

  /*Manage navigation into user */
  public pageChangeEventHander($event: any) {
    this.loadUser();
    this.displayedView = $event;
  }

  /*====The following functions are used by the left side menu buttons====*/

  public consultUserTimeEvent() {
    this.displayedView = "consultUserTime";
  }

  public onClickAddTime() {
    this.displayedView = "addUserTime";
  }

  /*close the left side menu */
  openEnd() {
    this.menu.close();
  }
}
